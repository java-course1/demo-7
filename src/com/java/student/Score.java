package com.java.student;

public class Score {

    int a;
    // this

    public Score () { // default Constructor
        this(1, "b");
        a = 40;
        System.out.println("Hello");
    }

    Score (int aa, String b) { //

        this.a = aa;
        System.out.println(a + b);
    }

    void print() {
        this.a = 6;
    }

    public static void main(String[] args) {


        Score score = new Score();
        score.a = 10;

        Score score1 = new Score();

        System.out.println(score.a);
    }
}
