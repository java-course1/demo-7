package com.java.student;

 class Student {

     static int age = 2;
     String name;

    // calculate student's average
    double calculateAverage(double khmer,
                            double math,
                            double physics,
                            double chemistry) {

        double sum = khmer + math + physics + chemistry;
        double avg = sum / 4;

        return avg;
    }

    // find student's grade
     static String calculateGrade (double average) {
        if (average < 50) {
            return "F";
        } else if (average < 60) {
            return  "E";
        } else if (average < 70) {
            return "D";
        } else if (average < 80) {
            return  "C";
        } else if (average < 90) {
            return "B";
        } else if (average < 100) {
            return "A";
        }
        System.out.println("finished");
        return "";
    }

    public static void main(String[] args) {
        Student student = new Student();
        double result = student.calculateAverage(60, 40, 56, 97);
        System.out.println("Result is : " + result);

        String grade = student.calculateGrade(result);
        System.out.println("Grade is : " + grade);

        String g = Student.calculateGrade(result);
        System.out.println(g);

        Student.age = 30;

        student.age = 10;

        Student student1 = new Student();
        student1.age = 20;

        System.out.println("age : " + Student.age);
        System.out.println("age 1 : " + Student.age);

    }


}

class StudentScore {

}
