package com.java.staticKeyword;

public class StaticDemo{

    //static can be used within variable, method, block , nested class

    //static variable
    static int age = 3;
    static int a = 4;


    // static method
    static void print () {
        System.out.println("Static Method");
    }

    // static block
    static {
        a = 200;
        System.out.println("static block");
    }


    public static void main(String[] args) {

        StaticDemo staticDemo = new StaticDemo();
        StaticDemo staticDemo1 = new StaticDemo();

        staticDemo.a = 5;
        staticDemo1.a = 9;

        StaticDemo.age = 6;

        System.out.println("Demo : " + staticDemo.a);
        System.out.println("Demo 1 : " + staticDemo1.a);

        StaticDemo.print();

        System.out.println("value of a : " + StaticDemo.a);

    }
}
