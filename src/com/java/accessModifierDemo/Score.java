package com.java.accessModifierDemo;

public class Score {
    void printScore() {
        System.out.println("default method");
    }

    protected void printProtectedMethod () {
        System.out.println("protected method in Score class");
    }

    public void printScore (int a) {
        System.out.println("public method");
    }


    public void printScore (int a, int b) {

    }
}
