package com.java;


import com.java.accessModifierDemo.Score;

public class Main {

    public static void main(String[] args) {
        // body
//        Main main = new Main();
//        main.printMessage("Private Message");

        Student student = new Student();
        student.printMessage("Hello");
        student.printProtectedMethod();


        Score score = new Score();
        Main main = new Main();
//        main.printProtectedMethod();
//        score.printScore();
//        score.printProtectedMethod();

        main.print();
        student.printStudent();
        score.printScore(1);

    }

    // modifier returnType nameOfMethod (parameter) { return expression;}

    public void print() {
        System.out.println("public method");
    }

}
