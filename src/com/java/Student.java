package com.java;

public class Student {

    // default method
     void printMessage(String message) {
        System.out.println(message);
        return;
    }

    protected void printProtectedMethod () {
        System.out.println("Protected Method");
    }

    // public
    public void printStudent() {
        System.out.println("print");
    }
}
