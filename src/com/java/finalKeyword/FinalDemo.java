package com.java.finalKeyword;

public class FinalDemo {

    // final variable
    final int maxValue;

    FinalDemo () {
        maxValue = 4; // we can initialize of final variable in constructor
    }

    // final method
    final void printA () {
        System.out.println("final method");
    }

    public static void main(String[] args) {

        FinalDemo finalDemo = new FinalDemo();

        System.out.println("Max Value : " + finalDemo.maxValue);
        finalDemo.printA();
    }
}
